MLOps course from ODS
==============================

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

# Getting started

## Install project
To manage project dependencies `poetry` is used.

1. Create conda environment:
```bash
conda create -n dimaonds python=3.9.13
```
2. Activate conda environment
```bash
conda activate diamonds
```
3. Install poetry
```bash
pip install poetry
```
4. Install poetry dependencies
```bash 
poetry install
```
5. Once new dependencies added to `pyproject.toml`, update the `poetry.lock` file with the following command:
```bash
poetry lock
```
6. Commit your updated file `poetry.lock`.

## Use DVC

To run DVC pipeline with `dvc.yaml` configuration, use the following command:
```bash
dvc exp run
```

## Use MLFlow
Model params, metrics and artifacts would be automatically saved locally in mlflow folder.
To observe experiments, use the MLFlow UI and follow the instructions appearing:
```bash
mlflow ui
```

## Local deployment
To deploy your model locally, use module `src.webserver` and `deploy.Dockerfile`.

1. To start deployment:
```bash
docker compose up --build
```

2. To stop deployment:
```bash
docker compose down
```
