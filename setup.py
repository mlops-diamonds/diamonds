"""Module to setup the project."""
from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="MLOps course from ODS",
    author="Your name (or your organization/company/team)",
    license="MIT",
)
