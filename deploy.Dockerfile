# Dockerfile for model deployment with FastAPI
# To run use the following command:
# docker build -t diamonds_deploy -f ./deploy.Dockerfile .
# Remember to attach `mlruns` folder to the container.

FROM python:3.9.13

WORKDIR /app

RUN pip install poetry==1.4.2
COPY pyproject.toml pyproject.toml
COPY poetry.lock poetry.lock
RUN poetry install --only deploy

COPY src/webserver/* /app/src/webserver/

# CMD ["poetry", "run", "uvicorn", "src.webserver.__main__:app", "--host", "0.0.0.0", "--port", "8000"]