import click
import pandas as pd


@click.command()
@click.option(
    "--input_file", type=click.Path(exists=True), help="Path to input csv file"
)
@click.option("--output_file", type=click.Path(), help="Path to output processed file")
def main(input_file: str, output_file: str):
    """
    Preprocessing and cleaning data
    :param input_file: Path to input csv file
    :param output_file: Path to output processed file
    """
    # Read file
    data = pd.read_csv(input_file, index_col=0)

    # Dropping dimensionless diamonds
    data = data.drop(data[data["x"] == 0].index)
    data = data.drop(data[data["y"] == 0].index)
    data = data.drop(data[data["z"] == 0].index)

    # Dropping the outliers.
    data = data[(data["depth"] < 75) & (data["depth"] > 45)]
    data = data[(data["table"] < 80) & (data["table"] > 40)]
    data = data[(data["x"] < 30)]
    data = data[(data["y"] < 30)]
    data = data[(data["z"] < 30) & (data["z"] > 2)]

    data = data.dropna(axis=0)
    data.to_csv(output_file, index=False)


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
