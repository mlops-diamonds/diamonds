import click
import pandas as pd

TEST_SIZE = 0.25
RANDOM_STATE = 7


@click.command()
@click.option(
    "--input_file", type=click.Path(exists=True), help="Path to input csv file"
)
@click.option("--train_file", type=click.Path(), help="Path for output train file")
@click.option("--test_file", type=click.Path(), help="Path for output test file")
@click.option(
    "--test_size", type=float, default=TEST_SIZE, help="Fraction of test data"
)
@click.option(
    "--random_state",
    type=int,
    default=RANDOM_STATE,
    help="Seed for random number generator",
)
def main(
    input_file: str,
    train_file: str,
    test_file: str,
    test_size: float,
    random_state: int,
):
    """
    :param input_file: Path to input csv file
    :param train_file: Path for output train file
    :param test_file: Path for output test file
    :param test_size: Fraction of test data
    :param random_state: Seed for random number generator
    """
    # Read file
    data = pd.read_csv(input_file)
    print(input_file)

    # Split data
    test_data = data.sample(frac=test_size, random_state=random_state)
    train_data = data[~data.index.isin(test_data.index)]

    # Save to files
    test_data.to_csv(test_file, index=False)
    train_data.to_csv(train_file, index=False)


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
