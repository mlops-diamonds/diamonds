import os

import mlflow
from fastapi import FastAPI

from src.webserver.models import Message

app = FastAPI()


RUN_ID = os.getenv("RUN_ID") or "500bb6cd8d58479b82f7f2f83bef3a87"
print(f"Model run id: {RUN_ID}")

# Deployment without Docker
# logged_model = f"runs:/{RUN_ID}/XGBoost_pipeline"
# Local folder attached to Docker
logged_model = f"/app/mlruns/0/{RUN_ID}/artifacts/XGBoost_pipeline"

# Load model as a PyFuncModel.
model = mlflow.pyfunc.load_model(logged_model)


@app.get("/")
async def root():
    return {"message": "Server is OK."}


@app.post("/predict")
async def predict(query: Message):
    raw_prediction = model.predict(query.features)
    prediction = [float(x) for x in raw_prediction]

    return {
        "message": "OK",
        "prediction": prediction,
    }
