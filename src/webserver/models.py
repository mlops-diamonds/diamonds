from typing import Optional

# pylint: disable=no-name-in-module
from pydantic import BaseModel


# pylint: disable=consider-alternative-union-syntax
class Message(BaseModel):
    tag: Optional[str] = None
    features: Optional[dict[str, object]]
