import click
import mlflow
import pandas as pd
import sklearn.metrics
from mlflow import log_metric, log_param
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from xgboost import XGBRegressor

CV = 10
FEATURES = ["carat", "clarity", "color", "cut", "depth", "table", "x", "y", "z"]
MAX_DEPTH = 3
RANDOM_SEED = 2023
SCORING = "neg_mean_absolute_percentage_error"
TARGET_NAME = "price"


def cut_target(
    dataset: pd.DataFrame, target_name: str, features: list[str] = None
) -> tuple[pd.DataFrame]:
    return (
        dataset.drop(target_name, axis=1)[features]
        if features
        else dataset.drop(target_name, axis=1),
        dataset[[target_name]],
    )


@click.command()
@click.option(
    "--train_data", type=click.Path(exists=True), help="Path to input train csv file"
)
@click.option("--test_data", type=click.Path(), help="Path to output model file")
def main(train_data: str, test_data: str):
    """
    Train the model
    :param train_data: Path to input train csv file
    :param test_data: Path to input test csv file
    """
    # Read file
    data = pd.read_csv(train_data)

    # Assigning the features as X and target as y
    features_train, target_train = cut_target(data, TARGET_NAME, features=FEATURES)

    # Building pipeline of standard scaler and model for regressor.
    pipeline_xgb = Pipeline(
        [
            ("scalar5", StandardScaler()),
            (
                "rf_classifier",
                XGBRegressor(max_depth=MAX_DEPTH, random_state=RANDOM_SEED),
            ),
        ]
    )

    # fits and evaluates model
    cv_score = cross_val_score(
        estimator=pipeline_xgb, X=features_train, y=target_train, scoring=SCORING, cv=CV
    )

    log_metric(SCORING, -cv_score.mean() if cv_score.mean() < 0 else cv_score.mean())
    log_param("Features", FEATURES)
    log_param("Number of folds", CV)
    log_param("Max depth", MAX_DEPTH)
    log_param("Random seed", RANDOM_SEED)

    # Check metrics on test data
    # Fit the pipeline
    pipeline_xgb.fit(features_train, target_train)

    # Read test data
    data_test = pd.read_csv(test_data)
    features_test, target_test = cut_target(data_test, TARGET_NAME, features=FEATURES)

    # Calculating metrics on test using the same metrics as during training
    scorer = sklearn.metrics.get_scorer(SCORING)
    metrics_test = scorer(pipeline_xgb, features_test, target_test)
    log_metric(SCORING + "_test", -metrics_test if metrics_test < 0 else metrics_test)

    # Log model with signature
    signature = mlflow.models.infer_signature(
        features_train, pipeline_xgb.predict(features_train)
    )
    mlflow.sklearn.log_model(pipeline_xgb, "XGBoost_pipeline", signature=signature)


if __name__ == "__main__":
    with mlflow.start_run() as run:
        print(f"Run ID: {run.info.run_id}")
        main()  # pylint: disable=no-value-for-parameter
        print("Success")
