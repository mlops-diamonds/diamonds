import click
import pandas as pd
from sklearn.preprocessing import LabelEncoder


@click.command()
@click.option(
    "--input_file", type=click.Path(exists=True), help="Path to input csv file"
)
@click.option("--output_file", type=click.Path(), help="Path to output csv file")
def main(input_file: str, output_file: str):
    """
    Encoding categorical data
    :param input_file: Path to input csv file
    :param output_file: Path to output csv file
    """
    # Read file
    data = pd.read_csv(input_file)
    object_cols = ["cut", "color", "clarity"]

    # Apply label encoder to each column with categorical data
    label_encoder = LabelEncoder()
    for col in object_cols:
        data[col] = label_encoder.fit_transform(data[col])

    # Save data
    data.to_csv(output_file, index=False)


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
